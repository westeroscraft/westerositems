package com.westeroscraft.westerositems;

import com.westeroscraft.westerositems.render.CustomItemRenderer;

import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;

public class ClientProxy extends Proxy {
	public ClientProxy() {
	}
	@Override
    public void registerCustomItemRenderer(Item itm) {
        MinecraftForgeClient.registerItemRenderer(itm, new CustomItemRenderer());
        System.out.println("Custom item render for item=" + itm.getUnlocalizedName());
    }

}
