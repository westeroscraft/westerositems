package com.westeroscraft.westerositems;

public class Version {
    public static final String VER = "@VERSION@";
    public static final String BUILD_NUMBER = "@BUILD_NUMBER@";
}

