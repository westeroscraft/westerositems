package com.westeroscraft.westerositems;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent; 
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import net.minecraft.crash.CrashReport;
import net.minecraft.util.ReportedException;
import net.minecraftforge.common.config.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.westeroscraft.westerositems.armor.WesterosArmorConfig;
import com.westeroscraft.westerositems.tools.WesterosToolConfig;

@Mod(modid = "WesterosItems", name = "WesterosItems", version = Version.VER)
public class WesterosItems
{    
    public static final String MOD_ID = "WesterosItems";
    public static Logger log = Logger.getLogger(MOD_ID);
    
    // The instance of your mod that Forge uses.
    @Instance("WesterosItems")
    public static WesterosItems instance;

    // Says where the client and server 'proxy' code is loaded.
    @SidedProxy(clientSide = "com.westeroscraft.westerositems.ClientProxy", serverSide = "com.westeroscraft.westerositems.Proxy")
    public static Proxy proxy;

    public boolean good_init = false;

    public WesterosArmorConfig armorConfig;
    public WesterosToolConfig toolConfig;
    
    public static void crash(Exception x, String msg) {
        CrashReport crashreport = CrashReport.makeCrashReport(x, msg);
        throw new ReportedException(crashreport);
    }
    public static void crash(String msg) {
        crash(new Exception(), msg);
    }
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        // Read our armor definition resource
        InputStream in = getClass().getResourceAsStream("/WesterosArmor.json");
        if (in == null) {
            crash("WesterosItems couldn't find its armor definition resource");
            return;
        }
        InputStreamReader rdr = new InputStreamReader(in);
        Gson gson = new Gson();
        try {
            armorConfig = gson.fromJson(rdr, WesterosArmorConfig.class);
        } catch (JsonSyntaxException iox) {
            crash(iox, "WesterosItems couldn't parse its armor definition");
            return;
        } catch (JsonIOException iox) {
            crash(iox, "WesterosItems couldn't read its armor definition");
            return;
        } finally {
            if (in != null) { try { in.close(); } catch (IOException iox) {}; in = null; }
        }
        // Read our tool definition resource
        in = getClass().getResourceAsStream("/WesterosTools.json");
        if (in == null) {
            crash("WesterosItems couldn't find its tool definition resource");
            return;
        }
        rdr = new InputStreamReader(in);
        gson = new Gson();
        try {
            toolConfig = gson.fromJson(rdr, WesterosToolConfig.class);
        } catch (JsonSyntaxException iox) {
            crash(iox, "WesterosItems couldn't parse its tool definition");
            return;
        } catch (JsonIOException iox) {
            crash(iox, "WesterosItems couldn't read its tool definition");
            return;
        } finally {
            if (in != null) { try { in.close(); } catch (IOException iox) {}; in = null; }
        }
        // Load configuration file - use suggested (config/WesterosBlocks.cfg)
        Configuration cfg = new Configuration(event.getSuggestedConfigurationFile());
        try
        {
            cfg.load();
            // Initialize armor configuration
            if (armorConfig.preInitialize(cfg) == false) {
                crash("WesterosItems couldn't preInitialize its armor configuration");
            }
            // Initialize tool configuration
            if (toolConfig.preInitialize(cfg) == false) {
                crash("WesterosItems couldn't preInitialize its tool configuration");
            }
            good_init = true;
        }
        catch (Exception e)
        {
            crash(e, "WesterosItems couldn't load its configuration");
        }
        finally
        {
            cfg.save();
        }
        // Initialize armor configuration
        if(armorConfig.initialize() == false) {
            crash("Error initializing armor");
        }
        log.info("Loaded " + armorConfig.armor.length + " armor definitions");
        // Initialize tool configuration
        if(toolConfig.initialize() == false) {
            crash("Error initializing tools");
        }
    }

    @EventHandler
    public void load(FMLInitializationEvent event) {
        if (!good_init) {
            crash("preInit failed - aborting load()");
            return;
        }
        log.info("Loaded " + toolConfig.tool.length + " tool definitions");
    }
        
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    }

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
    }
    
    @EventHandler
    public void serverStarted(FMLServerStartedEvent event) {
    }

    @EventHandler
    public void serverStopping(FMLServerStoppingEvent event) {
    }
}
