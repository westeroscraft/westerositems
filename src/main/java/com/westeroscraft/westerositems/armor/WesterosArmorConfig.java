package com.westeroscraft.westerositems.armor;

import java.util.HashMap;

import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.util.EnumHelper;

import com.westeroscraft.westerositems.WesterosItems;

import cpw.mods.fml.common.registry.LanguageRegistry;


public class WesterosArmorConfig {
    public WesterosArmorMaterialConfig[] material = new WesterosArmorMaterialConfig[0];
    public WesterosArmorItemConfig[] armor = new WesterosArmorItemConfig[0];
    
    public static HashMap<String, ArmorMaterial> customMaterials = new HashMap<String, ArmorMaterial>();
    public static WesterosArmorItem[] armorItems;
    // Call during preInit()
    public boolean preInitialize(Configuration cfg) {
        for (WesterosArmorMaterialConfig mat : material) {
            ArmorMaterial m = EnumHelper.addArmorMaterial(mat.materialName.toUpperCase(), mat.durability, 
                    new int[] { mat.damageReductionHelmet, mat.damageReductionChest, mat.damageReductionLegs, mat.damageReductionBoots },
                    mat.enchantability);
            if (m == null) {
                WesterosItems.log.severe("Error registering armor material : " + mat.materialName);
                return false;
            }
            customMaterials.put(mat.materialName.toUpperCase(), m);
        }
        // Add settings for armor definitions
        for (int i = 0; i < armor.length; i++) {
            if (armor[i].itemTexture.indexOf(':') < 0) {
                armor[i].itemTexture = "westerositems:" + armor[i].itemTexture;
            }
            if (armor[i].armorTexture.indexOf(':') < 0) {
                armor[i].armorTexture = "westerositems:" + armor[i].armorTexture;
            }
            if (armor[i].armorTexture.endsWith(".png") == false) {
                armor[i].armorTexture += ".png";
            }
        }
        return true;
}
    
    // Call during init()
    public boolean initialize() {
        armorItems = new WesterosArmorItem[armor.length];
        for (int i = 0; i < armor.length; i++) {
            armorItems[i] = new WesterosArmorItem(armor[i]);
        }
        return true;
    }
}
