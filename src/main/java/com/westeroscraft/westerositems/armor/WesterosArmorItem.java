package com.westeroscraft.westerositems.armor;

import com.westeroscraft.westerositems.WesterosItems;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class WesterosArmorItem extends ItemArmor {
    
    private WesterosArmorItemConfig cfg;
    
    public WesterosArmorItem(WesterosArmorItemConfig cfg) {
        super(cfg.getArmorMaterial(), 0, cfg.getArmorType());
        this.cfg = cfg;
        if (cfg.durability > 0) {
            this.setMaxDamage(cfg.durability);
        }
        this.maxStackSize = 1;
        this.setCreativeTab(CreativeTabs.tabCombat);
        setUnlocalizedName(cfg.itemName);
        setTextureName(cfg.itemTexture);
        GameRegistry.registerItem(this, cfg.itemName, WesterosItems.MOD_ID);
    }
    public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
    {
        return cfg.armorTexture;
    }
}
