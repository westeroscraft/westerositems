package com.westeroscraft.westerositems.armor;

import net.minecraft.item.ItemArmor.ArmorMaterial;

import com.westeroscraft.westerositems.WesterosItems;

public class WesterosArmorItemConfig {
    public String itemName;
    public String material = "iron";        // "cloth", "chain", "iron", "gold", "diamond"
    public String part = "";                // Which part of the armor: "helmet", "chest", "legs", "boots"
    public int durability = 0;              // Durability
    public String creativeTab = null;       // Creative tab for items
    public String itemTexture = "";         // Item texture
    public String armorTexture = "";        // Texture for armor
    
    public ArmorMaterial getArmorMaterial() {
        String m = material.toUpperCase();
        ArmorMaterial mat = ArmorMaterial.valueOf(m);
        if (mat == null) {
            mat = WesterosArmorConfig.customMaterials.get(m);
        }
        if (mat == null) {
            WesterosItems.log.info("Invalid armor material: " + material + " - using iron");
            mat = ArmorMaterial.IRON;
        }
        return mat;
    }
    public int getArmorType() {
        if (part.equalsIgnoreCase("helmet")) {
            return 0;
        }
        else if (part.equalsIgnoreCase("chest")) {
            return 1;
        }
        else if (part.equalsIgnoreCase("legs")) {
            return 2;
        }
        else if (part.equalsIgnoreCase("boots")) {
            return 3;
        }
        else {
            WesterosItems.log.info("Invalid armor part: " + part + " - using helmet");
            return 0;
        }
    }
}
