package com.westeroscraft.westerositems.armor;

public class WesterosArmorMaterialConfig {
    public String materialName;
    public int durability;  // Max damage before breaking
    public int damageReductionHelmet;   // Damage reduction for helmets
    public int damageReductionChest;    // Damage reduction for chestplate
    public int damageReductionLegs;     // Damage reduction for leggings
    public int damageReductionBoots;    // Damage reduction for boots
    public int enchantability;          // Enchantability
}
