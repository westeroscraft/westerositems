package com.westeroscraft.westerositems.tools;

import com.westeroscraft.westerositems.WesterosItems;
import com.westeroscraft.westerositems.render.ItemRenderInterface;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;

import org.lwjgl.opengl.GL11;

public class WesterosAxeItem extends ItemAxe implements ItemRenderInterface {
    public static class Factory extends WesterosToolFactory {
        @Override
        public Item buildToolClass(WesterosToolItemConfig cfg) {
            return new WesterosAxeItem(cfg);
        }
    }
    
    private WesterosToolItemConfig cfg;
    
    public WesterosAxeItem(WesterosToolItemConfig cfg) {
        super(cfg.getToolMaterial());
        this.cfg = cfg;
        if (cfg.maxUses > 0)
            this.setMaxDamage(cfg.maxUses);
        if (cfg.weaponDamage > 0.0)
            //TODO - this.damageVsEntity = cfg.weaponDamage;
        this.setUnlocalizedName(cfg.itemName);
        this.setTextureName(cfg.itemTexture);
        GameRegistry.registerItem(this, cfg.itemName, WesterosItems.MOD_ID);
        if (cfg.scale != 1.0F)
            WesterosItems.proxy.registerCustomItemRenderer(this);
    }

    @Override
    public void renderSpecial() {
        if (this.cfg.scale != 1.0F) {
            GL11.glScalef(this.cfg.scale, this.cfg.scale, 1.0F);
        }
    }
}
