package com.westeroscraft.westerositems.tools;

import com.westeroscraft.westerositems.WesterosItems;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBow;
import net.minecraft.util.IIcon;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class WesterosBowItem extends ItemBow {
    public static class Factory extends WesterosToolFactory {
        @Override
        public Item buildToolClass(WesterosToolItemConfig cfg) {
            return new WesterosBowItem(cfg);
        }
    }
    
    private WesterosToolItemConfig cfg;
    private IIcon[] iconArray;
    
    public WesterosBowItem(WesterosToolItemConfig cfg) {
        super();
        this.cfg = cfg;
        if (cfg.maxUses > 0)
            this.setMaxDamage(cfg.maxUses);
        this.setUnlocalizedName(cfg.itemName);
        this.setTextureName(cfg.itemTexture);
        GameRegistry.registerItem(this, cfg.itemName, WesterosItems.MOD_ID);
    }
    
    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IIconRegister iconReg)
    {
        this.itemIcon = iconReg.registerIcon(cfg.itemTexture);
        this.iconArray = new IIcon[3];

        for (int i = 0; (i < cfg.inUseTextures.length) && (i < 3); ++i)
        {
            this.iconArray[i] = iconReg.registerIcon(cfg.inUseTextures[i]);
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getItemIconForUseDuration(int par1)
    {
        return this.iconArray[par1];
    }
}
