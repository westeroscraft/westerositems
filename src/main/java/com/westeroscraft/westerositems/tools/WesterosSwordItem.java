package com.westeroscraft.westerositems.tools;

import org.lwjgl.opengl.GL11;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSword;

import com.google.common.collect.Multimap;
import com.westeroscraft.westerositems.WesterosItems;
import com.westeroscraft.westerositems.render.ItemRenderInterface;

import cpw.mods.fml.common.registry.GameRegistry;

public class WesterosSwordItem extends ItemSword implements ItemRenderInterface {
    public static class Factory extends WesterosToolFactory {
        @Override
        public Item buildToolClass(WesterosToolItemConfig cfg) {
            return new WesterosSwordItem(cfg);
        }
    }
    
    private WesterosToolItemConfig cfg;
    
    public WesterosSwordItem(WesterosToolItemConfig cfg) {
        super(cfg.getToolMaterial());
        this.cfg = cfg;
        if (cfg.maxUses > 0)
            this.setMaxDamage(cfg.maxUses);
        this.setUnlocalizedName(cfg.itemName);
        this.setTextureName(cfg.itemTexture);
        GameRegistry.registerItem(this, cfg.itemName, WesterosItems.MOD_ID);
        if (cfg.scale != 1.0F)
            WesterosItems.proxy.registerCustomItemRenderer(this);
    }
    @Override
    public Multimap getItemAttributeModifiers()
    {
        Multimap multimap = super.getItemAttributeModifiers();
        if (cfg.weaponDamage > 0.0F) {
            multimap.put(SharedMonsterAttributes.attackDamage.getAttributeUnlocalizedName(), new AttributeModifier(field_111210_e, "Weapon modifier", (double)cfg.weaponDamage, 0));
        }
        return multimap;
    }
    @Override
    public void renderSpecial() {
        if (this.cfg.scale != 1.0F) {
            GL11.glScalef(this.cfg.scale, this.cfg.scale, 1.0F);
        }
    }
}
