package com.westeroscraft.westerositems.tools;

import java.util.HashMap;

import com.westeroscraft.westerositems.WesterosItems;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.util.EnumHelper;


public class WesterosToolConfig {
    public WesterosToolMaterialConfig[] material = new WesterosToolMaterialConfig[0];
    public WesterosToolItemConfig[] tool = new WesterosToolItemConfig[0];
    
    public static HashMap<String, ToolMaterial> customMaterials = new HashMap<String, ToolMaterial>();
    public static HashMap<String, WesterosToolFactory> toolTypes = new HashMap<String, WesterosToolFactory>();
    public static Item[] toolItems;
    // Call during preInit()
    public boolean preInitialize(Configuration cfg) {
        customMaterials.put("DIAMOND", ToolMaterial.EMERALD);   // Add alias for diamond
        for (WesterosToolMaterialConfig mat : material) {
            ToolMaterial m = EnumHelper.addToolMaterial(mat.materialName.toUpperCase(), mat.harvestLevel, 
                    mat.maxUses, mat.efficiencyOnProperMaterial, mat.damageVsEntity, mat.enchantability);
            if (m == null) {
                WesterosItems.log.severe("Error registering tool material : " + mat.materialName);
                return false;
            }
            customMaterials.put(mat.materialName.toUpperCase(), m);
        }
        // Add tool types
        toolTypes.put("sword", new WesterosSwordItem.Factory());
        toolTypes.put("bow", new WesterosBowItem.Factory());
        toolTypes.put("axe", new WesterosAxeItem.Factory());
        
        // Add settings for item definitions
        for (int i = 0; i < tool.length; i++) {
            if (tool[i].itemTexture.indexOf(':') < 0) {
                tool[i].itemTexture = "westerositems:" + tool[i].itemTexture;
            }
            if (tool[i].inUseTextures != null) {
                for (int j = 0; j < tool[i].inUseTextures.length; j++) {
                    if (tool[i].inUseTextures[j].indexOf(':') < 0) {
                        tool[i].inUseTextures[j] = "westerositems:" + tool[i].inUseTextures[j];
                    }
                }
            }
        }
        return true;
}
    
    // Call during init()
    public boolean initialize() {
        toolItems = new Item[tool.length];
        for (int i = 0; i < tool.length; i++) {
            WesterosToolFactory fact = toolTypes.get(tool[i].itemType);
            if (fact == null) {
                WesterosItems.log.severe("Error registering tool " + tool[i].itemName + " : bad type - " + tool[i].itemType);
                return false;
            }
            toolItems[i] = fact.buildToolClass(tool[i]);
            if (toolItems[i] == null) {
                WesterosItems.log.severe("Error registering tool " + tool[i].itemName);
                return false;
            }
        }
        return true;
    }
}
