package com.westeroscraft.westerositems.tools;

import net.minecraft.item.Item;

// Abstract factory class : each custom tool type needs to have one
public abstract class WesterosToolFactory {
    /* Build instance of given tool type
     * 
     * @param def - definition loaded for tool item
     * @returns item based on definition
     */
    public abstract Item buildToolClass(WesterosToolItemConfig def);
}
