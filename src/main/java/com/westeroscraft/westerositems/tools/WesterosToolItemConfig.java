package com.westeroscraft.westerositems.tools;

import net.minecraft.item.Item.ToolMaterial;

import com.westeroscraft.westerositems.WesterosItems;

public class WesterosToolItemConfig {
    public String itemName;
    public String itemType;                 // Item type - "sword", "axe", etc
    public String material = "iron";        // "wood", "stone", "iron", "emerald", "gold", "diamond"
    public int maxUses = 0;                 // Max uses
    public String creativeTab = null;       // Creative tab for items
    public String itemTexture = "";         // Item texture
    public String[] inUseTextures = null;   // In use textures (for box)
    public float weaponDamage = 0.0F;       // Weapon damage
    public float scale = 1.0F;
    
    public ToolMaterial getToolMaterial() {
        String m = material.toUpperCase();
        ToolMaterial mat = ToolMaterial.valueOf(m);
        if (mat == null) {
            mat = WesterosToolConfig.customMaterials.get(m);
        }
        if (mat == null) {
            WesterosItems.log.info("Invalid tool material: " + material + " - using iron");
            mat = ToolMaterial.IRON;
        }
        return mat;
    }
}
