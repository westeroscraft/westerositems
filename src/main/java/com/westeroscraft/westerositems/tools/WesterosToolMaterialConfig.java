package com.westeroscraft.westerositems.tools;

public class WesterosToolMaterialConfig {
    public String materialName;
    public int harvestLevel;    // Harvest level of tool (3 = DIAMOND, 2 = IRON, 1 = STONE, 0 = IRON/GOLD)
    public int maxUses;         // Max uses of item build with material
    public float efficiencyOnProperMaterial;    // Efficiency of item on blocks matching one of its proper material types
    public float damageVsEntity;    // Base damage for item made with material versus mobs/players
    public int enchantability;  // Enchantability
}
